<!--
  Copyright (c) 2006-2018, JGraph Ltd
  
  Hello, World! example for mxGraph. This example demonstrates using
  a DOM node to create a graph and adding vertices and edges.
-->
<html>
<head>
	<title>Hello, World! example for mxGraph</title>

	<!-- Sets the basepath for the library if not in same directory -->
	<script type="text/javascript">
		mxBasePath = 'src';
	</script>

	<!-- Loads and initializes the library -->
	<script type="text/javascript" src="src/js/mxClient.js"></script>

	<!-- Example code -->
	<script type="text/javascript">

    // Program starts here. Creates a sample graph in the
    // DOM node with the specified ID. This function is invoked
    // from the onLoad event handler of the document (see below).
    function main(container)
    {
        // Checks if the browser is supported
        if (!mxClient.isBrowserSupported())
        {
            // Displays an error message if the browser is not supported.
            mxUtils.error('Browser is not supported!', 200, false);
        }
        else
        {
            // Disables the built-in context menu
            mxEvent.disableContextMenu(container);

            // Creates the graph inside the given container
            var graph = new mxGraph(container);

    //                var config = mxUtils.load('editors/config/keyhandler-commons.xml').getDocumentElement();
    //                var editor = new mxEditor(config);

            // Enables rubberband selection
            new mxRubberband(graph);

            // Gets the default parent for inserting new cells. This
            // is normally the first child of the root (ie. layer 0).
            var parent = graph.getDefaultParent();

            // Adds cells to the model in a single step
            graph.getModel().beginUpdate();
            try
            {
                var v_start = graph.insertVertex(parent, null, 'Start', 200, 20, 100, 100, "shape=ellipse");
                var v_middle = graph.insertVertex(parent, null, 'Click twice do change value', 150, 200, 200, 100);
                var v_end = graph.insertVertex(parent, null, 'End', 200, 400, 100, 100, "shape=doubleEllipse");
                var e_start_middle = graph.insertEdge(parent, null, '', v_start, v_middle);
                var e_middle_end = graph.insertEdge(parent, null, '', v_middle, v_end);
            }
            finally
            {
                // Updates the display
                graph.getModel().endUpdate();
            }

            var button_xml = mxUtils.button('View XML', function()
            {
                var encoder = new mxCodec();
                var node = encoder.encode(graph.getModel());
                mxUtils.popup(mxUtils.getPrettyXml(node), true);
            });
            
            var button_json = mxUtils.button('View JSON', function()
            {
                var encoder = new mxCodec();
                var node = encoder.encode(graph.getModel());
                var xml = mxUtils.getPrettyXml(node);
                mxUtils.popup(mxUtils.getPrettyJson(xml), true);
            });

            document.body.insertBefore(button_xml, container.nextSibling);
            document.body.insertBefore(button_json, container.nextSibling);
        }
    };	
    </script>
</head>

<!-- Page passes the container for the graph to the program -->
<body onload="main(document.getElementById('graphContainer'))"
      style="text-align: center;">

	<!-- Creates a container for the graph with a grid wallpaper -->
	<div id="graphContainer"
		style="position:relative;overflow:hidden;width:100%;height:90%;background:url('editors/images/grid.gif');cursor:default;">
	</div>
</body>
</html>
